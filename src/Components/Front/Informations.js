import React from "react";
import "../../Sass/components/_informations.scss";
import Fade from 'react-reveal/Fade';
import { Link } from "react-router-dom"

import HistoPicto from "../../Img/icone/AssoIcon.svg"
import BadgeIcon from "../../Img/icone/BadgeIcon.svg"
import UserIcon from "../../Img/icone/UserIcon.svg"

import HistoriIC from "../../Img/illustration/HistoriIC.svg"
import Association from "../../Img/illustration/Asso.svg"
import Badge from "../../Img/illustration/Badge.svg"



export default class Informations extends React.Component {
    render(){
        return(
            <section id="informations">
                <span></span>
                <div className="container">
                    {/* <DataCard 
                        title="Exemple" text="text d'exemple" icon={Water} className="picto" alt="Title"
                        illu={Pshit} className2="illustration" alt2="Title"
                    /> */}
                    <Fade top>
                        <div className="row" id="asso">
                            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                                <h2>L’association</h2>
                                <p>Infoclimat est une association à but non lucratif régie par la loi de 1903 créée le 15 octobre 2003 par les co-auteurs du site internet www.infoclimat.fr dont la création remonte au 7 Octobre 2001.</p>
                                <a href="https://asso.infoclimat.fr/" rel="noreferrer" target="_blank" title="Asso Infoclimat">Découvrir l'association</a>
                            </div>

                            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                                <img src={UserIcon} className="picto" alt="picto"/>
                            </div>

                            <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                                <img src={Association} className="illustration effect-up" alt="eau"/>
                            </div>
                        </div>
                    </Fade>

                    <Fade top delay={500}>
                    <div className="row" id="ic">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={HistoriIC} className="illustration effect-up" alt="eau"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={HistoPicto} className="picto" alt="picto"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <h2>Qu’est-ce qu’HistorIC ?</h2>
                            <p>HistorIC, c'est l'archivage de 2729 événements météorologiques extrêmes en France et 225091 séries de valeurs depuis 1653</p>
                            <Link to="/map" className="cta-no-gradient">Visualisation en direct</Link>
                        </div>
                    </div>
                    </Fade>

                    <Fade top delay={500}>
                    <div className="row" id="badge">
                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <h2>Nos Badges</h2>
                            <p>Si vous êtes passionnées, vous pouvez contribuer avec vos informations et 
vos clichés, de plus vous avez la possibilité de récupérer des graphiques et des statistiques gratuitement.</p>
                            <Link to="/map" className="cta-no-gradient">Nos badges</Link>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={BadgeIcon} className="picto" alt="picto"/>
                        </div>

                        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
                            <img src={Badge} className="illustration effect-up" alt="eau"/>
                        </div>
                    </div>
                    </Fade>
                </div>
            </section>
        )
    }
}