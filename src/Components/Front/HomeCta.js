import { Link } from "react-router-dom"

const HomeCta = () => {
    return (  
        <Link to="/map" className="cta-gradient">Commencer</Link>
    );
}
 
export default HomeCta;