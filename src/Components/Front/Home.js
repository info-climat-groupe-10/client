import React from 'react'
import { useSpring, animated } from 'react-spring'
import { Link } from 'react-scroll'
import "../../Sass/components/_home.scss";
import "../../Sass/components/_cta.scss";
import "../../Sass/components/_parallaxe.scss";
import Earth from "../../Img/video/earth.mp4";
import Illustration from "../../Img/illustration/IlluGraph.svg"
import HomeCta from "./HomeCta";

function Home() {  
  const [props, set] = useSpring(() => ({ xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }))
  const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2]

  // Losange
  const trans1 = (x, y) => `translate3d(${x / 10}px,${y / 10}px,0)`
  // Cercle Blur
  const trans2 = (x, y) => `translate3d(${x / 8 + 35}px,${y / 8 - 230}px,0)`
  // Triangle
  const trans4 = (x, y) => `translate3d(${x / 3.5}px,${y / 3.5}px,0)`
  // Losange
  const trans5 = (x, y) => `translate3d(${x / 2 + 400}px,${y / 8 - 230}px,0)`
  // Triangle Blur
  const trans6 = (x, y) => `translate3d(${x / 4 + 600}px,${y / 8 - 20}px,0)`
  
    return (
      <section id="home">
        <div id="filter"></div>
        <video autoPlay loop muted id="video">
          <source src={Earth} type="video/mp4"/>
        </video>
        <div className="container pr">
          <div className="row">
            <div className="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <header>
                <Link className="cta" to="informations" spy={true} smooth={true} duration={2500} >
                  L'association
                </Link>

                <Link className="cta" to="ic" spy={true} smooth={true} duration={2500} >
                  Qu’est-ce qu’HistorIC
                </Link>

                <Link className="cta" to="badge" spy={true} smooth={true} duration={2500} >
                  Nos badges
                </Link>
                </header>
            </div>
          </div>
          <div className="row full" onMouseMove={({ clientX: x, clientY: y }) => set({ xy: calc(x, y) })}>
            <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
              <h1>L'HistorIC, la météo partout en France en temps réel.</h1>
                <animated.div className="icon1" style={{ transform: props.xy.interpolate(trans1) }} />
                <animated.div className="icon2" style={{ transform: props.xy.interpolate(trans2) }} />
              <p>Découvrez tous les événements extrême météorologique en France et DOM TOM 
grâce à notre carte intéractive avec  une interface agréable à l'utilisation,
vous pouvez filtrer à votre guise pour retrouver votre information exacte. </p>
              <HomeCta/>
            </div>
            <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 align-self-center">
              <img src={Illustration} alt="illustration"/>
                <animated.div className="icon4" style={{ transform: props.xy.interpolate(trans4) }} />
                <animated.div className="icon5" style={{ transform: props.xy.interpolate(trans5) }} />
                <animated.div className="icon6" style={{ transform: props.xy.interpolate(trans6) }} />
            </div>
          </div>
        </div>
      </section>
    );
  }

export default Home