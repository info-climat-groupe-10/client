import React, { useRef, useEffect, useState } from "react";
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import { ApiConfig } from "../../Utils/ApiConfig";
import "../../Sass/components/_maps.scss";
import { Helmet } from "react-helmet";

import IleDeFrance from "../../Data/Regions/IleDeFrance.geojson";
import AuvergneRhoneAlpes from "../../Data/Regions/AuvergneRhoneAlpes.geojson";
import BourgogneFrancheComte from "../../Data/Regions/BourgogneFrancheComte.geojson";
import Bretagne from "../../Data/Regions/Bretagne.geojson";
import CentreValDeLoire from "../../Data/Regions/CentreValDeLoire.geojson";
import Corse from "../../Data/Regions/Corse.geojson";
import GrandEst from "../../Data/Regions/GrandEst.geojson";
import Guadeloupe from "../../Data/Regions/Guadeloupe.geojson";
import Guyane from "../../Data/Regions/Guyane.geojson";
import HautsDeFrance from "../../Data/Regions/HautsDeFrance.geojson";
import LaReunion from "../../Data/Regions/LaReunion.geojson";
import Martinique from "../../Data/Regions/Martinique.geojson";
import Mayotte from "../../Data/Regions/Mayotte.geojson";
import Normandie from "../../Data/Regions/Normandie.geojson";
import NouvelleAquitaine from "../../Data/Regions/NouvelleAquitaine.geojson";
import Occitanie from "../../Data/Regions/Occitanie.geojson";
import PaysDeLaLoire from "../../Data/Regions/PaysDeLaLoire.geojson";
import ProvenceAlpesCoteDAzur from "../../Data/Regions/ProvenceAlpesCoteDAzur.geojson";
import circle from "../../Img/icons/circle.svg";
import Tornade from "../../Img/icons/Tornade.svg";
import Neige from "../../Img/icons/Neige.svg";
import Gele from "../../Img/icons/Gele.svg";
import Pluie from "../../Img/icons/Pluie.svg";
import Chaleur from "../../Img/icons/Chaleur.svg";
import Grêle from "../../Img/icons/Grêle.svg";
import Autre from "../../Img/icons/Autre.svg";
import Inondation from "../../Img/icons/Inondation.svg";
import Eclair from "../../Img/icons/Eclair.svg";

import LineCharts from "../Charts/LineCharts.js"

import Parser from "html-react-parser";
import {getData} from "../../Utils/getData";

mapboxgl.accessToken = ApiConfig.mapboxgl_accessToken;

const Carte = () => {
    const mapContainer = useRef(null);
    const map = useRef(null);
    const [lng, setLng] = useState(2.3);
    const [lat, setLat] = useState(46.1);
    const [zoom, setZoom] = useState(5);
    const [data, setData] = useState([]);
    const [tabYear, setTabYear] = useState([]);
    const [error, setError] = useState("");
    const [arboCountry] = useState("France");
    const [arboRegion, setArboRegion] = useState({
        name: "",
        center: [],
        zoom: 0,
    });

    const regions = [
        {
            name: "idf",
            geoJson: IleDeFrance,
            center: [2.5, 48.55],
            zoom: 7.7,
        },
        {
            name: "auvergne_rhone_alpes",
            geoJson: AuvergneRhoneAlpes,
            center: [4.4908, 45.5144],
            zoom: 6.6,
        },
        {
            name: "bourgogne_france_comte",
            geoJson: BourgogneFrancheComte,
            center: [4.7838, 47.2941],
            zoom: 6.7,
        },
        {
            name: "bretagne",
            geoJson: Bretagne,
            center: [-3, 48.1],
            zoom: 7,
        },
        {
            name: "centre_val_de_loire",
            geoJson: CentreValDeLoire,
            center: [1.6985, 47.6475],
            zoom: 6.5,
        },
        {
            name: "corse",
            geoJson: Corse,
            center: [9.1046, 42.1966],
            zoom: 7.3,
        },
        {
            name: "grand_est",
            geoJson: GrandEst,
            center: [5.7744, 48.85],
            zoom: 6.5,
        },
        {
            name: "guadeloupe",
            geoJson: Guadeloupe,
            center: [-61.4497, 16.1317],
            zoom: 8.9,
        },
        {
            name: "guyane",
            geoJson: Guyane,
            center: [-53.3133, 3.8337],
            zoom: 6.6,
        },
        {
            name: "haut_de_france",
            geoJson: HautsDeFrance,
            center: [2.9, 49.989],
            zoom: 6.8,
        },
        {
            name: "la_reunion",
            geoJson: LaReunion,
            center: [55.5351, -21.1371],
            zoom: 8.9,
        },
        {
            name: "martinique",
            geoJson: Martinique,
            center: [-60.9975, 14.6282],
            zoom: 9.4,
        },
        {
            name: "mayotte",
            geoJson: Mayotte,
            center: [45.1593, -12.8355],
            zoom: 9.8,
        },
        {
            name: "normandie",
            geoJson: Normandie,
            center: [0.0485, 49.1308],
            zoom: 7,
        },
        {
            name: "nouvelle_aquitaine",
            geoJson: NouvelleAquitaine,
            center: [0.0878, 45.0095],
            zoom: 5.9,
        },
        {
            name: "occitanie",
            geoJson: Occitanie,
            center: [2.1387, 43.6926],
            zoom: 6.6,
        },
        {
            name: "pays_de_la_loire",
            geoJson: PaysDeLaLoire,
            center: [-0.801, 47.3975],
            zoom: 6.7,
        },
        {
            name: "provence_alpes_cote_d_azur",
            geoJson: ProvenceAlpesCoteDAzur,
            center: [6.1763, 44.089],
            zoom: 6.9,
        },
    ];

    useEffect(() => {
        if (map.current) return; // initialize map only once
        map.current = new mapboxgl.Map({
            container: mapContainer.current,
            style: "mapbox://styles/kwasque/ckpwq8a9581jk18psuc1bitb5",
            center: [lng, lat],
            zoom: zoom,
        });
        if (!map.current) return; // wait for map to initialize
        map.current.on("move", () => {
            setLng(map.current.getCenter().lng.toFixed(4));
            setLat(map.current.getCenter().lat.toFixed(4));
            setZoom(map.current.getZoom().toFixed(2));
        });

        map.current.on("load", () => {
            map.current.addControl(new mapboxgl.NavigationControl());

            // toggleSidebar('right'); // initialisation de l'ouverture
            // map.current.scrollZoom.disable();
            // map.current.dragging.disable();
            regions.forEach((region) => {
                map.current.addSource(region.name, {
                    type: "geojson",
                    data: region.geoJson,
                    generateId: true
                });

                map.current.addLayer({
                    id: region.name + "Fill",
                    type: "fill",
                    source: region.name,
                    layout: {
                        visibility: "visible",
                    },
                    paint: {
                        "fill-color": "#0072FF",
                        "fill-opacity": [
                            "case",
                            ["boolean", ["feature-state", "hover"], false],
                            0.5,
                            0.2,
                        ],
                    },
                });

                map.current.addLayer({
                    id: region.name + "Border",
                    type: "line",
                    source: region.name,
                    layout: {
                        visibility: "visible",
                    },
                    paint: {
                        "line-color": "#0072FF",
                        "line-width": [
                            "case",
                            ["boolean", ["feature-state", "weight"], false],
                            3,
                            1,
                        ],
                    },
                });

                map.current.on("mousemove", region.name + "Fill", (e) => {
                    document.body.style.cursor = "pointer";
                    map.current.setFeatureState(
                        { source: region.name, id: 0 },
                        { hover: true }
                    );
                });

                map.current.on("mouseleave", region.name + "Fill", (e) => {
                    document.body.style.cursor = "default";
                    map.current.setFeatureState(
                        { source: region.name, id: 0 },
                        { hover: false }
                    );
                });

                map.current.on("click", region.name + "Fill", (e) => {
                    //
                    // ZOOM SUR UNE REGION
                    //
                    Jump(region.center, region.zoom);
                    regions.forEach((region2) => {
                        map.current.setLayoutProperty(
                            region2.name + "Fill",
                            "visibility",
                            "none"
                        );
                        map.current.setLayoutProperty(
                            region2.name + "Border",
                            "visibility",
                            "none"
                        );
                    });
                    map.current.setFeatureState(
                        { source: region.name, id: 0 },
                        { weight: true }
                    );
                    map.current.setLayoutProperty(
                        region.name + "Border",
                        "visibility",
                        "visible"
                    );

                    setTimeout(() => {
                        var elem =
                            document.getElementsByClassName("sidebar")[0];
                        var classes = elem.className.split(" ");

                        classes.splice(classes.indexOf("collapsed"), 1);

                        setTimeout(() => {
                            document.getElementById("charts_map").classList.add("notHide");
                        }, 1200);
                    
                        map.current.easeTo({
                            padding: { right: 400, bottom: 100 },
                            duration: 1000,
                        });

                        elem.className = classes.join(" ");

                        getPins(region.name);
                        setArboRegion({
                            name: region.name,
                            center: region.center,
                            zoom: region.zoom,
                        });
                    }, 1000);
                });
            });
        });
    });

    function getPins(regionName) {
        getData(`${ApiConfig.json_server_bdd + "/" + regionName} `)
            .then((res) => {
                var tabYearTemp = {}
                setTabYear([])

                res.forEach(event => {
                    if (tabYearTemp[event.date[0].split('/')[2]] !== undefined) {
                        tabYearTemp[event.date[0].split('/')[2]] ++;
                    } else {
                        tabYearTemp[event.date[0].split('/')[2]] = 1;
                    }
                })

                setTabYear(tabYearTemp)

                setData(res);
                displayPins(res);
                var actualId = "";

                var returnedFunction = debounce(function () {
                    if (
                        document
                            .querySelector(".events")
                            .classList.contains("opened")
                    ) {
                        for (var i = 0; i < res.length; i++) {
                            if (isElementOnScreen(res[i].id)) {
                                if (actualId === res[i].id) break;
                                actualId = res[i].id;
                                const flyToTemp = {
                                    center: res[i].center,
                                    bearing:
                                        Math.floor(Math.random() * 90) - 45,
                                    zoom: 11,
                                    pitch: Math.floor(Math.random() * 90) - 45,
                                    speed: 1,
                                };
                                map.current.flyTo(flyToTemp);
                                break;
                            }
                        }
                    }
                }, 200);

                var elements = document.getElementsByClassName('event');

                var i = 0;

                function myLoop() {         
                    setTimeout(function() {   
                        if (elements[i] !== undefined) elements[i].classList.add('notHide');   
                        i++;                    
                        if (i < elements.length && i < 20) {           
                            myLoop();             
                        } else {
                            for (var j = i; j < elements.length; j++) {
                                elements[j].classList.add('notHide');   
                            }
                        }             
                    }, 100)
                }

                setTimeout(function() {  
                    myLoop();       
                }, 500)              


                document
                    .getElementsByClassName("sidebar-content")[0]
                    .addEventListener("scroll", returnedFunction);
            })
            .catch((err) => {
                setError(err);
            });
    }

    function Jump(center, zoom) {
        map.current.flyTo({
            center: center,
            zoom: zoom,
            pitch: 0,
            bearing: 0,
            essential: true,
        });
    }

    function removeElementsByClass(className) {
        const elements = document.getElementsByClassName(className);
        while (elements.length > 0) {
            elements[0].parentNode.removeChild(elements[0]);
        }
    }

    function Back() {

        var elements = document.getElementsByClassName('pins');

        var i = 0;

        function myLoop() {         
            setTimeout(function() {   
                if (elements[i] !== undefined) elements[i].classList.remove('notHide');   
                i++;                    
                if (i < elements.length) {           
                    myLoop();             
                } else {
                    removeElementsByClass("pins");
                }               
            }, 30)
        }

        setTimeout(function() {  
            myLoop();       
        }, 500)    
        setData([]);
        var elem = document.getElementsByClassName("sidebar")[0];
        var classes = elem.className.split(" ");
        classes.push("collapsed");

        document.getElementById("charts_map").classList.remove("notHide");

        map.current.easeTo({
            padding: { right: 0 },
            duration: 1000,
        });

        elem.className = classes.join(" ");

        setTimeout(() => {
            Jump([2.3, 46.1], 5);
            regions.forEach((region2) => {
                map.current.setLayoutProperty(
                    region2.name + "Fill",
                    "visibility",
                    "visible"
                );
                map.current.setLayoutProperty(
                    region2.name + "Border",
                    "visibility",
                    "visible"
                );
                map.current.setFeatureState(
                    { source: region2.name, id: 0 },
                    { weight: false }
                );
            });
            document.querySelector(".events").classList.remove("opened");
        }, 1000);
    }

    function displayPins(pins) {
        pins.forEach((pin) => {
            var el = document.createElement("img");
            el.className = "pins";
            if (pin.type === 'Tempête/coup de vent' || pin.type === 'Tornade' || pin.type === 'Cyclone') {
                el.src = Tornade;
            } else if (pin.type === 'Froid' || pin.type === 'Froid inhabituel' || pin.type === 'Gêlées tardives' || pin.type === 'Gêlées précoces') {
                el.src = Gele
            } else if (pin.type === 'Épisode neigeux' || pin.type === 'Épisode neigeux tardif' || pin.type === 'Épisode neigeux précoce') {
                el.src = Neige
            } else if (pin.type === 'Pluie verglaçantes' || pin.type === 'Épisode pluvieux') {
                el.src = Pluie
            } else if (pin.type === 'Inondation') {
                el.src = Inondation
            } else if (pin.type === 'Chaleur / canicule' || pin.type === 'Sécheresse') {
                el.src = Chaleur
            } else if (pin.type === 'Orages') {
                el.src = Eclair
            } else if (pin.type === 'Grêle') {
                el.src = Grêle
            } else {
                el.src = Autre
            }

            if (!isNaN(pin.center[0]) && !isNaN(pin.center[1]))
                new mapboxgl.Marker(el).setLngLat(pin.center).addTo(map.current);

            el.addEventListener("click", (e) => {
                openEvent(pin);
            });
        });

        pins = document.getElementsByClassName('pins');

        var i = 0;

        function myLoop() {         
            setTimeout(function() {   
                if (pins[i])
                    pins[i].classList.add('notHide');   
                i++;                    
                if (i < pins.length) {           
                myLoop();             
                }                       
            }, 100)
        }


        setTimeout(function() {  
            myLoop();                   
        }, 500)    
    }

    function openEvent(pin) {
        document.querySelector(".events").classList.add("opened");
        document.getElementById(pin.id).scrollIntoView();
        var nodes = Array.prototype.slice.call( document.querySelector('.events').children );

        if(nodes.indexOf(document.getElementById(pin.id)) === 1) {
            const flyToTemp = {
                center: pin.center,
                bearing:
                    Math.floor(Math.random() * 90) - 45,
                zoom: 11,
                pitch: Math.floor(Math.random() * 90) - 45,
                speed: 1,
            };
            map.current.flyTo(flyToTemp);
        }
    }

    function clickEvent(pin) {
        openEvent(pin);
    }

    function isElementOnScreen(id) {
        var element = document.getElementById(id);
        if (element) {
            var bounds = element.getBoundingClientRect();
            return bounds.top < window.innerHeight && bounds.bottom > 20;
        }
    }

    function goToRegion(region) {
        removeElementsByClass("pins");
        getPins(region.arboRegion.name);
        Jump(region.arboRegion.center, region.arboRegion.zoom);
        document.querySelector(".events").classList.remove("opened");
    }



    function getPositions(center) {
        removeElementsByClass("pins");
        var res = [];
        for (var i = 0; i < 20; i++){
            res.push({
                "id": i,
                "Description": "Description",
                "center": [center[0] + (Math.floor(Math.random() * 90) - 45) / 800,  center[1] + (Math.floor(Math.random() * 90) - 45) / 800],
                "date": ["02/02/1990"]
            })
        } 
        res.forEach((position) => {
            var el = document.createElement("img");
            el.className = "pins";
            el.src = circle;

            new mapboxgl.Marker(el).setLngLat([position.center[0],  position.center[1]]).addTo(map.current);

            var pins = document.getElementsByClassName('pins');

            var i = 0;

            function myLoop() {         
                setTimeout(function() {   
                    pins[i].classList.add('notHide');   
                    i++;                    
                    if (i < pins.length) {           
                        myLoop();             
                    }                       
                }, 100)
            }

            setTimeout(function() {  
                myLoop();                   
            }, 500)    
        })
    }

    const debounce = (func, wait) => {
        let timeout;

        return function executedFunction(...args) {
            const later = () => {
                clearTimeout(timeout);
                func(...args);
            };

            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    };

    return (
        <div>
            <Helmet>
                <title>InfoClimat - Carte</title>
            </Helmet>
            <div id="geocoder" className="geocoder"></div>
            <div id="map" ref={mapContainer} className="map-container" />
            <div id="charts_map">
                {Object.keys(tabYear).length > 0 && <LineCharts dataChart={tabYear}/>}
            </div>
            <div className="sidebar collapsed">
                <div className="sidebar-content">
                    <React.Fragment>
                        <section className="events">
                            <div className="arbo">
                                <span className="link" onClick={() => Back()}>
                                    {arboCountry}
                                </span>
                                <span> / </span>
                                <span
                                    className="link"
                                    onClick={() => goToRegion({ arboRegion })}
                                >
                                    {arboRegion.name}
                                </span>
                            </div>
                            {error ? (
                                <p>{error.message}</p>
                            ) : (
                                data.map((getData, index) => (
                                    <div
                                        key={index}
                                        className="event"
                                        id={getData.id}
                                        onClick={() => clickEvent(getData)}
                                    >
                                        <div>{getData.type ? getData.type : 'Pas de nom' }</div>
                                        <div className={Parser(getData.importance) + ' badge'}>
                                            {Parser(getData.importance)}
                                        </div>
                                        {/* <p>{getData.coord[0]}</p>
                                        <p>{getData.coord[1]}</p> */}
                                        <div className="date">
                                            {getData.date[0]} {getData.date[1] ? " - " + getData.date[1] : "" }
                                        </div>
                                        <div className="description">
                                            Lorem ipsum dolor sit amet,
                                            consectetur adipiscing elit. Sed non
                                            risus. Suspendisse lectus tortor,
                                            dignissim sit amet, adipiscing nec,
                                            ultricies sed, dolor. Cras elementum
                                            ultrices diam. Maecenas ligula
                                            massa, varius a, semper congue,
                                            euismod non, mi. Proin porttitor,
                                            orci nec nonummy molestie, enim est
                                            eleifend mi, non fermentum diam nisl
                                            sit amet erat.

                                            <span onClick={() => getPositions(getData.center)}> En savoir plus... </span>
                                        </div>
                                    </div>
                                ))
                            )}
                        </section>
                    </React.Fragment>
                </div>
            </div>
        </div>
    );
};

export default Carte;
