import React, { useState } from "react";
import { Bar } from "react-chartjs-2";

const genData = () => ({
    labels: [],
    datasets: [
        {
            type: "line",
            label: "Nombre d'évenement par année",
            backgroundColor: "#36a2eb",
            data: [],
        },
    ],
});



const options = {
    scales: {
        yAxes: [
            {
                ticks: {
                    beginAtZero: true,
                },
            },
        ],
    },
};

const data = genData();

const LineChart = (props) => {
    const [clickedElement, setClickedElement] = useState("");

    var tabYear = [],
        yearKey = [];

    var maxYear = 0,
        minYear = 0,
        i = 0;
    for (const [key] of Object.entries(props.dataChart)) {
        if (i === 0) {
            minYear = key;
        } else if (i === Object.keys(props.dataChart).length - 1) {
            maxYear = key;
        }
        i++;
    }
    for (i = minYear; i < maxYear; i++) {
        tabYear.push(i);
        yearKey.push(props.dataChart[i]);
    }
    data.labels = tabYear;
    data.datasets[0].data = yearKey;


    const getElementAtEvent = (element) => {
        if (!element.length) return;

        const { datasetIndex, index } = element[0];
        setClickedElement(`${data.labels[index]} - ${data.datasets[datasetIndex].data[index]}`);
    };

    return (
        <>
            <div className="badge-chart Exceptionnel">
                <p>{clickedElement}</p>
            </div>
            <Bar height={20} width={100} data={data} options={options} getElementAtEvent={getElementAtEvent} />
        </>
    );
};

export default LineChart;
