import React, { lazy, Suspense } from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Style
import "../Sass/components/_reset.scss"
import "../Sass/modules/_typographie.scss"

// Loader
import Loader from "../Components/Loader/Main"

// Import Js
const Main = lazy(() => import("../Pages/Main"));
const Map = lazy(() => import("../Components/Map/Carte"));
const NotFound = lazy(() => import("./NotFound"));


const Rooter = () => {
    return (
        <Suspense fallback={<Loader/>}>    
            <Router>
                <Switch>
                    <Route exact path="/" component={Main} />
                    <Route exact path="/map" component={Map} />
                    <Route path="*" component={NotFound} />
                </Switch>
            </Router>   
        </Suspense>          
    )
};

export default Rooter