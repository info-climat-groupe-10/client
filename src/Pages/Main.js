// import { Link } from "react-router-dom"
// import {Helmet} from "react-helmet";
//import Typography from '@material-ui/core/Typography';
// import Home from "../Img/Home.svg"
// import Eclipse from "../Img/Ellipse.svg"
// import Association from "../Img/Association.svg"
// import Historic from "../Img/Historic.svg"
// import Badges from "../Img/Badges.svg"
//import Fade from 'react-reveal/Fade';

import Home from "../Components/Front/Home";
import Informations from '../Components/Front/Informations';

export default function Main() {
    return(
      <>
        <Home/>
        <Informations/>
      </>
    )
}