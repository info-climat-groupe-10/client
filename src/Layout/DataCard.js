import React from 'react'
import "../../Sass/Components/_dataCard.scss"

const DataCard = ({title, text, icon, illu, className, className2, alt, alt2}) => (
    <div className="row" id="DataCard">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
            <h2>{title}</h2>
            <p>{text}</p>
        </div>

        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
            <img src={icon} className={className} alt={alt}/>
        </div>

        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 align-self-center">
            <img src={illu} className={className2} alt={alt2}/>
        </div>
    </div>
)

export default DataCard