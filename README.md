# Présentation

Ce projet a été organisé dans le cadre du hackaton 5e année du [réseau GES](https://www.reseau-ges.fr/) ([ESGI](https://www.esgi.fr/) [ECITV](https://www.ecitv.fr/)), édtion 2021.  
Cette application a pour vocation d'exploiter à minima les données de l'association [Infoclimat](https://www.infoclimat.fr/).  
Il s'agit d'une API Rest, permettant seulement d'exploiter les données (seulement de la récupération via la méthode HTTP GET).

# Stack technique

Pour exploiter les données fourni par l'association, la stack suivante a été mise en place:

- [React Js](https://fr.reactjs.org/): framework front-end
- [Docker](https://www.docker.com/): environnement de développement conteneurisé

# Installation
## Commandes

- ```docker-compose build:``` Installation de l'environnement de développement et des dépendances
- ```docker-compose up:``` Lancement des containers docker
- ```npm run bdd:``` Lancement de la bdd local
- ```docker-compose stop:``` Arrêt les containers


https://infoclimat.netlify.app/ -> besoins de npm run bdd (branche main)
https://pprod-infoclimat.netlify.app -> data avec api (branche mainWithData)