start:
	- docker-compose up

compile:
	- docker-compose build

react-build:
	- docker-compose exec react npm run build
up-build:
	- docker-compose up --build

fix:
	- docker-compose exec react npm audit fix --force
stop:
	- docker-compose down

test:
	- npx cypress run
	# or 
	# npm cypress open
	# or
	# docker run -it -v $PWD:/e2e -w /e2e cypress/included:3.2.0

analyze:
	- docker-compose exec react npm run analyze
	
netlify-pprod:
	-  netlify build && netlify deploy

netlify-prod:
	-  netlify build && netlify deploy --prod

sass:
	- docker-compose exec react npm uninstall node-sass \
	- docker-compose exec react npm i node-sass \
	- docker-compose exec react npm rebuild node-sass